#王荟玲MD语法笔记
## 一.标题
标题是最常用的语法，如果我们想将一段文字或者词语变为标题时，我们需要在句首打上一个“#”紧接着敲一个空格。每加一个“#”，字体都会小一号。  
原代码  
# 一级标题
## 二级标题  
示例如下  
 ![image](http://note.youdao.com/iyoudao/wp-content/uploads/2016/09/2-0-%E6%A0%87%E9%A2%98.png) 
 ---
## 二.分割线  
 分割线可以使两段内容明显地被分开，更易区别。  
 要获得分割线，只需要在第一段文字下另起一行并连续输入“***”即可。   
 原代码
 ---

 示例如下  
 ![image](http://note.youdao.com/iyoudao/wp-content/uploads/2016/09/2-5-%E5%88%86%E5%89%B2%E7%BA%BF.png)
 ---
## 三.引用  
 如果想要在一段文字中引用某些文章或名言也有固定的语法。在引用的句子前加上"&gt;"再加空格即可。  
 原代码  
 &gt; 引用的一句话
 
 示例如下  
 ![image](http://note.youdao.com/iyoudao/wp-content/uploads/2016/09/2-2%E5%BC%95%E7%94%A8.png)
 ---
## 四.列表  
 列表的使用可以让我们的书面表达更加清晰，而列表又分为有序列表和无序列表。无序列表的固定语法是在文字前加上"-"再加空格。如果想获得有序列表则在文字前加上“1.2.3.”。  
 原代码  
 #### 无序列表
 - 列表1
 - 列表2
    - 列表2.1
#### 有序列表
1.  列表1
    1.  列表1.1
    2.  列表2.1
2. 列表2  
示例如下。  
 ![image](http://note.youdao.com/iyoudao/wp-content/uploads/2016/09/2-1-%E5%88%97%E8%A1%A8.png)   
ps：列表也可分为几级，在一级列表下一行的列表前多加几个空格即刻变为下一级列表  
---
## 五.粗体和斜体  
 在MD语法中字体也可变化，分为粗体和斜体  
 粗体语法为：两个 * 夹着一段文本  
 斜体语法为：一个 * 夹着一段文本  
原代码  
**这是粗体**  
*这是斜体*  
 示例如下（*后面不需要空格）
 ![image](http://note.youdao.com/iyoudao/wp-content/uploads/2016/09/2-3-%E7%B2%97%E4%BD%93%E6%96%9C%E4%BD%93.png)
---
## 六.表格  
 表格的语法较复杂  
原代码  
header 1 | header 2   
---|---    
示例如下  
![image](http://note.youdao.com/iyoudao/wp-content/uploads/2016/09/1-3%E8%A1%A8%E6%A0%BC.png)
## 七.插入链接与图片
原代码  
插入链接语法：[文本]（链接）  
插入图片语法：[文本]（图片地址链接）  
示例如下
![image](http://note.youdao.com/iyoudao/wp-content/uploads/2016/09/2-4%E9%93%BE%E6%8E%A5%E4%B8%8E%E5%9B%BE%E7%89%87.png)